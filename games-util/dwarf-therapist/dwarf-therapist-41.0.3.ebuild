# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake-utils xdg

DESCRIPTION="Dwarf management tool for Dwarf Fortress"
HOMEPAGE="https://github.com/Dwarf-Therapist/Dwarf-Therapist"
SRC_URI="https://github.com/Dwarf-Therapist/Dwarf-Therapist/archive/v${PV}.tar.gz -> ${P}.tar.gz"

MY_PN="Dwarf-Therapist"
MY_PV="${PV}"
MY_P="${MY_PN}-${MY_PV}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	dev-qt/qtcore
	dev-qt/qtdeclarative
	dev-qt/qtconcurrent
	"
RDEPEND="
	${DEPEND}
	games-roguelike/dwarf-fortress
	"

S="${WORKDIR}/${MY_P}"

PATCHES=( "$FILESDIR/${PF}" )

src_prepare() {
	xdg_environment_reset

	cmake-utils_src_prepare
}
